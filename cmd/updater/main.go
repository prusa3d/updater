// SPDX-License-Identifier: GPL-2.0-only

package main

import (
	"flag"
	"os"
	"text/tabwriter"

	"github.com/go-playground/log"
	"github.com/go-playground/log/handlers/console"
	"github.com/google/uuid"

	"gitlab.com/prusa3d/go-omaha/omaha/updater"
)

const (
	OK = iota
	ERROR_USAGE

	DEFAULT_SERVER = "http://srv-upd-001.prusa/service/update2"
	DEFAULT_TRACK  = "stable"
	DEFAULT_APP_ID = "56a29876-1218-4fe9-8c0d-af1216c52828"
)

var (
	printHelp bool
	server    string
	track     string
	appID     string
	onDemand  bool

	out     *tabwriter.Writer
	flagSet *flag.FlagSet

)

func init() {
	out = new(tabwriter.Writer)
	out.Init(os.Stdout, 0, 8, 1, '\t', 0)

	flagSet = flag.NewFlagSet("updater", flag.ContinueOnError)
	flagSet.StringVar(&server, "server", DEFAULT_SERVER, "URL to Omaha update service")
	flagSet.StringVar(&track, "track", DEFAULT_TRACK, "Channel from which to feed the updates")
	flagSet.StringVar(&appID, "app-id", DEFAULT_APP_ID, "UUID of the application/system to by updated")
	flagSet.BoolVar(&printHelp, "help", false, "Print usage information and exit.")
	flagSet.BoolVar(&onDemand, "on-demand", true, "Run an on-demand check right after startup.")
}

func main() {
	var err error

	// once any other logger is registered the default logger is removed.
	cLog := console.New(true)
	log.AddHandler(cLog, log.AllLevels...)

	if err = flagSet.Parse(os.Args[1:]); err != nil {
		os.Exit(ERROR_USAGE)
	}
	if printHelp {
		flagSet.PrintDefaults()
		os.Exit(OK)
	}

	checker, err := updater.NewChecker(server, uuid.MustParse(appID), track)
	if err != nil {
		panic(err)
	}

	s := updater.NewService(checker)
	s.Run()
}
