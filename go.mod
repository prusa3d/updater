module gitlab.com/prusa3d/go-omaha

go 1.13

require (
	github.com/blang/semver v3.5.1+incompatible
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/go-playground/errors v1.3.1-0.20190511183022-b661b75d4162 // indirect
	github.com/go-playground/log v0.0.0-20190521043315-fc0587c376ea
	github.com/go-playground/pkg v1.0.2-0.20190522230805-792a755e6910 // indirect
	github.com/godbus/dbus/v5 v5.0.3-0.20190910164622-6eb4cf171088
	github.com/google/uuid v1.1.1
	github.com/kylelemons/godebug v1.1.0
	golang.org/x/crypto v0.0.0-20190911031432-227b76d455e7 // indirect
	golang.org/x/sys v0.0.0-20190913121621-c3b328c6e5a7 // indirect
)
