// SPDX-License-Identifier: GPL-2.0-only

package updater

import (
	"fmt"
)

func getSizeAndUnit(size float64, base float64, _map []string) (float64, string) {
	i := 0
	unitsLimit := len(_map) - 1
	for size >= base && i < unitsLimit {
		size = size / base
		i++
	}
	return size, _map[i]
}

// BytesSize returns a human-readable size in bytes, kibibytes,
// mebibytes, gibibytes, or tebibytes (eg. "44kiB", "17MiB").
func BytesSize(size float64) string {
	binaryAbbrs := []string{"B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"}
	size, unit := getSizeAndUnit(size, 1024.0, binaryAbbrs)
	return fmt.Sprintf("%.4g %s", size, unit)
}