// SPDX-License-Identifier: GPL-2.0-only
package updater

type UpdateInfo struct {
	CurrentVersion string
	NextVersion    string
	URL            string
	Digest         []byte
	Date           int64
}