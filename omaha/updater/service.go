// SPDX-License-Identifier: GPL-2.0-only

package updater

import (
	"crypto/sha1"
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"
	"os"
	"syscall"
	"time"

	"github.com/cavaliercoder/grab"
	"github.com/go-playground/log"
	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
	"github.com/godbus/dbus/v5/prop"

	"gitlab.com/prusa3d/go-omaha/omaha"
	"gitlab.com/prusa3d/go-omaha/omaha/client"
)

const (
	DBusPath      = "/cz/prusa3d/Updater1"
	DBusInterface = "cz.prusa3d.Updater1"
	DL_TIMEOUT	  = 10 * time.Second
)

type service struct {
	checker       *Checker
	info          UpdateInfo
	resp          *omaha.UpdateResponse
	status        *Status
	notes         *os.File
	props   	  *prop.Properties
	conn    	  *dbus.Conn
	grabResp	  *grab.Response
	startDownload chan interface{}
}

func NewService(checker *Checker) *service {
	var err error

	s := &service{
		checker: checker,
	}

	s.conn, err = dbus.SystemBus()
	if err != nil {
		panic(err)
	}

	reply, err := s.conn.RequestName(DBusInterface, dbus.NameFlagDoNotQueue)
	if err != nil {
		panic(err)
	}
	if reply != dbus.RequestNameReplyPrimaryOwner {
		fmt.Fprintln(os.Stderr, "name already taken")
		os.Exit(1)
	}
	propsSpec := map[string]map[string]*prop.Prop{
		DBusInterface: {
			"Status": &prop.Prop{
				Value:    Status{Operation: UpdateStatusIdle},
				Writable: true,
				Emit:     prop.EmitTrue,
				Callback: nil,
			},
			"UpdateInfo": &prop.Prop{
				Value:    UpdateInfo{},
				Writable: true,
				Emit:     prop.EmitFalse,
				Callback: nil,
			},
		},
	}
	s.props, err = prop.Export(s.conn, DBusPath, propsSpec)
	if err != nil {
		panic(err)
	}

	err = s.conn.Export(s, DBusPath, DBusInterface)
	if err != nil {
		panic(err)
	}

	err = s.conn.Export(s.introspect(), DBusPath,"org.freedesktop.DBus.Introspectable")
	if err != nil {
		panic(err)
	}

	s.status = NewStatus(s.props)

	return s
}

func (s *service) CancelDownload() *dbus.Error {
	if s.grabResp == nil || s.grabResp.IsComplete() {
		return dbus.MakeFailedError(errors.New("No download is currently in progress."))
	}
	if s.grabResp.HTTPResponse == nil {
		log.Debug("HTTPResponse == nil");
		return nil
	}
	if err := s.grabResp.HTTPResponse.Body.Close(); err != nil {
		log.Infof("Error returned when closing HTTP response body: %+v", err)
	}
	log.Notice("Download cancelled.")
	return nil
}

func (s *service) Check() *dbus.Error {
	s.checker.Demand()
	return nil
}

func (s *service) Download() *dbus.Error {
	if s.status.Operation != UpdateStatusUpdateAvailable {
		return dbus.MakeFailedError(errors.New("Nothing to download"))
	}
	if s.startDownload == nil {
		return dbus.MakeFailedError(errors.New("Download already started"))
	}
	close(s.startDownload)
	return nil
}

func (s *service) downloadPackage(bundlePath string) (err error) {
	fmt.Println("Downloading ", s.info.URL, " to ", bundlePath)

	if err = os.Remove(bundlePath); err != nil {
		if e, ok := err.(*os.PathError); ok && e.Err != syscall.ENOENT {
			return err
		}
	}
	gClient := &grab.Client{
		UserAgent: "updater",
		HTTPClient: &http.Client{
			Transport: http.DefaultTransport,
		},
	}

	req, err := grab.NewRequest(bundlePath, s.info.URL)
	if err != nil {
		return err
	}
	req.SetChecksum(sha1.New(), s.info.Digest, true)
	s.grabResp = gClient.Do(req)

	s.status.Downloading(0)
	t := time.NewTicker(time.Second)
	timeSinceLastTransfer := s.grabResp.Start

	defer t.Stop()
	for {
		select {
		case <-t.C:
			log.Infof("%.02f%% complete, %s/s",
					  100.0*s.grabResp.Progress(),
					  BytesSize(s.grabResp.BytesPerSecond()))
			s.status.Downloading(s.grabResp.Progress())
			if s.grabResp.BytesPerSecond() > 0 {
				timeSinceLastTransfer = time.Now()
			} else if time.Now().Sub(timeSinceLastTransfer) > DL_TIMEOUT {
				log.Errorf("No new data were received during last %v.", DL_TIMEOUT)
				_ = s.grabResp.HTTPResponse.Body.Close()
				return s.grabResp.Cancel()
			}
		case <-s.grabResp.Done:
			if err = s.grabResp.Err(); err != nil {
				log.Errorf("Download failed: %+v", err)
				return err
			}
			log.Infof("Update bundle downloaded to %s", bundlePath)
			s.status.Downloaded(bundlePath)
			if err = s.conn.Emit(DBusPath, "cz.prusa3d.Updater1.Downloaded", bundlePath); err != nil {
				log.Errorf("Encountered an error when emitting Downloaded signal: %+v", err)
			}

			return
		}
	}
}

func (s *service) GetReleaseNotes() (fd dbus.UnixFD, err *dbus.Error) {
	if s.notes != nil {
		return dbus.UnixFD(s.notes.Fd()), nil
	}
	log.Warnf("")
	return dbus.UnixFD(0), dbus.MakeFailedError(errors.New("There are no release notes available."))
}

func (s *service) Run() {
	s.checker.Demand()
	for {
		updateResponse, resp := s.checker.Check(s.status)
		for {
			if err := s.processUpdateResponse(updateResponse, resp); err == nil {
				break
			}
		}
	}
}

func (s *service) processUpdateResponse(updResp *omaha.UpdateResponse, resp *omaha.Response) (err error) {
	s.resp = updResp
	filename := updResp.Manifest.Packages[0].Name
	digest, err := base64.StdEncoding.DecodeString(updResp.Manifest.Packages[0].SHA1)
	if err != nil {
		log.Error(err)
		return
	}

	bundleDir, err := client.EnsureRuntimeDirectory()
	if err != nil {
		panic(err)
	}
	bundlePath := bundleDir + "/update.raucb"

	s.notes, err = os.OpenFile(bundleDir+"/release-notes", os.O_CREATE|syscall.O_RDWR, 0644)
	if err != nil {
		panic(err)
	}
	_, err = s.notes.WriteString(updResp.Manifest.ReleaseNotes.Data)
	if err != nil {
		panic(err)
	}
	notesPath := s.notes.Name()
	err = s.notes.Close()
	s.notes, err = os.Open(notesPath)
	if err != nil {
		panic(err)
	}

	s.info.URL = updResp.URLs[0].CodeBase + filename
	s.info.Digest = digest
	s.info.NextVersion = updResp.Manifest.Version
	s.info.Date = resp.DayStart.Time().Unix()
	s.info.CurrentVersion = client.VersionString()
	s.props.SetMust(DBusInterface, "UpdateInfo", dbus.MakeVariant(s.info))

	s.status.SetOperation(UpdateStatusUpdateAvailable)

	s.startDownload = make(chan interface{})
	select {
	case <-s.startDownload:
		s.startDownload = nil
		err = s.downloadPackage(bundlePath)
		if err != nil {
			s.status.DownloadFailed()
			log.Errorf("Bailing out")
			return err
		}
		break
	}
	return nil
}

func (s *service) introspect() introspect.Introspectable {
	n := &introspect.Node{
		Name: DBusPath,
		Interfaces: []introspect.Interface{
			introspect.IntrospectData,
			prop.IntrospectData,
			{
				Name:       DBusInterface,
				Methods:    introspect.Methods(s),
				Properties: s.props.Introspection(DBusInterface),
				Signals: []introspect.Signal{
					{
						Name: "Downloaded",
						Args: []introspect.Arg{
							{
								Name: "Path",
								Type: "s",
							},
						},
					},
				},
			},
		},
	}
	return introspect.NewIntrospectable(n)
}