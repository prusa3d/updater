package updater

import (
	"fmt"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/prop"
)

type StatusOperation uint32

const (
	UpdateStatusIdle = iota
	UpdateStatusCheckingForUpdate
	UpdateStatusUpdateAvailable
	UpdateStatusDownloading
	UpdateStatusDownloaded
	UpdateStatusDownloadFailed
	UpdateStatusUpToDate
	UpdateStatusCheckFailed
)

func (o StatusOperation) String() string {
	return [...]string{
		"Idle",
		"Checking for Update",
		"Update Available",
		"Downloading",
		"Downloaded",
		"Download Failed",
		"Up to Date",
		"Check Failed",
	}[o]
}

type Status struct {
	Operation		StatusOperation
	Progress		float64
	BundlePath		string
	props   	  	*prop.Properties
}

func NewStatus(props *prop.Properties) *Status {
	status := &Status {
		Operation: UpdateStatusIdle,
		Progress: 0,
		BundlePath: "",
		props: props,
	}
	status.commit()
	return status
}

func (s *Status) Downloaded(path string) {
	s.Operation = UpdateStatusDownloaded
	s.BundlePath = path
	s.Progress = 1
	s.commit()
}

func (s *Status) Downloading(progress float64) {
	s.Operation = UpdateStatusDownloading
	s.Progress = progress
	s.BundlePath = ""
	s.commit()
}

func (s *Status) DownloadFailed() {
	s.Operation = UpdateStatusDownloadFailed
	s.BundlePath = ""
	s.Progress = 0
	s.commit()
}

func (s *Status) SetOperation(op StatusOperation) {
	s.Operation = op
	s.BundlePath = ""
	s.Progress = 0
	s.commit()
}

func (s *Status) String() string {
	return fmt.Sprintf("Operation=%v Progress=%v", s.Operation, s.Progress)
}

func (s *Status) commit() {
	s.props.SetMust(DBusInterface, "Status", dbus.MakeVariant(s))
}