package updater

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-playground/log"
	"github.com/google/uuid"

	"gitlab.com/prusa3d/go-omaha/omaha"
	"gitlab.com/prusa3d/go-omaha/omaha/client"
)

type Checker struct {
	mc *client.Client
	c *client.AppClient
	sigc chan os.Signal
}

func NewChecker(server string, appID uuid.UUID, track string) (*Checker, error) {
	mc, err := client.NewMachineClient(server)
	if err != nil {
		panic(err)
	}
	c, err := mc.NewAppClient(appID)
	if err != nil {
		panic(err)
	}
	if err = c.SetTrack(track); err != nil {
		panic(err)
	}
	// Client version is the name and version of this updater.
	//c.SetClientVersion("go-omaha-0.0.1")

	checker := &Checker{
		mc: mc,
		c: c,
		sigc: make(chan os.Signal, 1),
	}
	signal.Notify(checker.sigc, syscall.SIGUSR1)
	log.Notice("To trigger an on-demand update check, send this process a SIGUSR1 signal.")

	return checker, nil
}

func (checker *Checker) Demand() {
	checker.sigc <- syscall.SIGUSR1 // Fake it
}

func (checker *Checker) Check(status *Status) (*omaha.UpdateResponse, *omaha.Response) {
	var update *omaha.UpdateResponse = nil
	for update == nil {
		duration := checker.c.DurationUntilNextPing()
		log.Infof("Scheduled next check in %s.", duration.Truncate(time.Second).String())

		var source string
		select {
		case <-checker.sigc:
			source = "ondemandupdate"
		case <-checker.c.NextPing():
			source = "scheduler"
		}

		status.SetOperation(UpdateStatusCheckingForUpdate)

		log.Debugf("Checking for a new version as requested by %s.", source)

		var err error;
		update, err = checker.c.UpdateCheck(source)
		if err == omaha.NoUpdate {
			status.SetOperation(UpdateStatusUpToDate)
			log.Info("No update is currently available.")
		} else if err != nil {
			status.SetOperation(UpdateStatusCheckFailed)
			log.Error(err)
		}
	}
	log.Infof("Received new update")
	return update, checker.c.GetLastResponse()
}