package client

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	"github.com/blang/semver"
	"github.com/google/uuid"
)

const (
	MachineIDPath 	= "/etc/machine-id"
	BootIDPath    	= "/proc/sys/kernel/random/boot_id"
	OSReleasePath 	= "/usr/lib/os-release"
	NvmemPath	  	= "/sys/bus/nvmem/devices/sunxi-sid0/nvmem"
	MacOffset		= 0x18
	SnOffset		= 0x3c
)

// Populate reads the os-release file, parses it, and
// sets fields for use.
func ReadVersion() (semver.Version, error) {
	var ver semver.Version
	// Iterate over our known file paths
	var rawContent []byte
	rawContent, _ = ioutil.ReadFile(OSReleasePath)

	// Error if we were unable to get any content
	if len(rawContent) == 0 {
		return ver, errors.New("No content available from os-release files")
	}

	var versionStr string

	for _, line := range strings.Split(string(rawContent), "\n") {
		if !strings.Contains(line, "=") {
			continue
		}
		item := strings.Split(line, "=")
		if item[0] != "VERSION_ID" {
			continue
		}
		// Remove prefix/suffix quotes from values
		item[1] = strings.TrimPrefix(item[1], "\"")
		item[1] = strings.TrimSuffix(item[1], "\"")
		// Set the field
		versionStr = item[1]
	}
	return semver.Parse(versionStr)
}

func VersionString() string {
	ver, _ := ReadVersion()
	return ver.String()
}

func ReadHWID() (uuid.UUID, error) {
	sid, err := ioutil.ReadFile(NvmemPath)
	if err != nil {
		return uuid.New(), err
	}
	snSlice := sid[SnOffset:(SnOffset+8)]
	macSlice := sid[MacOffset:(MacOffset+8)]

	return uuid.FromBytes(append(snSlice, macSlice...))
}

// IsDirWriteable checks if dir is writable by writing and removing a file
// to dir. It returns nil if dir is writable.
func IsDirWriteable(dir string) error {
	f := filepath.Join(dir, ".touch")
	if err := ioutil.WriteFile(f, []byte(""), 0600); err != nil {
		return err
	}
	return os.Remove(f)
}

func EnsureRuntimeDirectory() (dir string, err error) {
	var found bool
	if dir, found = syscall.Getenv("RUNTIME_DIRECTORY"); !found {
		dir = "/tmp"
	}
	if err := IsDirWriteable(dir); err != nil {
		return "", err
	}
	return dir, nil
}