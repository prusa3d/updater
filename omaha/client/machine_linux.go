// Copyright 2017 CoreOS, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// +build linux

package client

import (
	"bytes"
	"fmt"
	"io/ioutil"

	"github.com/go-playground/log"
	"github.com/google/uuid"
)

// NewMachineClient creates a machine-wide client, updating applications
// that may be used by multiple users. On Linux the system's machine id
// is used as the user id, and boot id is used as the omaha session id.
func NewMachineClient(serverURL string) (*Client, error) {
	machineID, err := ioutil.ReadFile(MachineIDPath)
	if err != nil {
		err = fmt.Errorf("omaha: failed to read machine id: %v", err)
		fmt.Println(err.Error())
	}

	machineUUID, err := uuid.Parse(string(bytes.TrimSpace(machineID)[:32]))
	if err != nil {
		err = fmt.Errorf("omaha: failed to read machine id: %v", err)
		fmt.Println(err.Error())
	}

	bootIDBytes, err := ioutil.ReadFile(BootIDPath)
	if err != nil {
		err = fmt.Errorf("omaha: failed to read boot id: %v", err)
		fmt.Println(err.Error())
	}

	bootID, err := uuid.ParseBytes(bytes.TrimSpace(bootIDBytes))
	if err != nil {
		err = fmt.Errorf("omaha: failed to parse boot id: %v", err)
		fmt.Println(err.Error())
		return nil, err
	}

	osVersion, err := ReadVersion()
	if err != nil {
		err = fmt.Errorf("omaha: failed to read VERSION_ID from os-release file: %v", err)
		fmt.Println(err.Error())
		return nil, err
	}
	log.Infof("Detected OS version: %v", osVersion)

	hwID, err := ReadHWID()
	if err != nil {
		hwID = machineUUID
	}

	c := &Client{
		apiClient:     newHTTPClient(),
		clientVersion: osVersion.String(),
		userID:        machineUUID,
		sessionID:     bootID,
		hwID:          hwID,
		isMachine:     true,
		apps:          make(map[uuid.UUID]*AppClient),
	}

	if err := c.SetServerURL(serverURL); err != nil {
		return nil, err
	}

	return c, nil
}

func (ac AppClient) GetVersion() string {
	return ac.clientVersion
}